**Shop Exercise**

Run it with ./mvnw spring-boot:run

---

## Assumptions / Considerations

As offer deletion is not mentioned, but cancellation, I assume the offers wants to be maintained in the DB, so I have used DELETE /offers/id as,
 currently, that's the effect for all users, but internally I am using expiryDate to keep the logic and maintaining the offer.
I assume in the future, there will be merchants with access to expired/cancelled offers.

Get Offer: Assumed we don't need NOT_FOUND/GONE response separation, for expired and not-existing offers, but I considered the option.

### Entities structure

 I can imagine further requirements will require having OfferDao and ProductDao, to differentiate the Offer (price, dates) from Name/Description.
 I have assumed, for simplicity, only one entity class is needed, but I have included Offer/OfferDao separation.

### Documentation

  I could have used Swagger or other library to help documenting the API endpoints.

### Persistence
  Using an embedded database, along with Spring JPA.

### Users / Security

  No authentication nor authorization considerations has been taken into account, although there would be at least 2 users. Merchant and User.
  Ideally, in the future, user could only read information, while merchant has access to write operations.

### Logging

  Basic logging incorporated.

### Caching

  Considered, but not done as it was not part of the exercise.