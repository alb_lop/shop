package com.alblop.shop;

import com.alblop.shop.offers.Offer;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class OfferApiTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void whenListingOffers_thenEmptyListIsReturned() throws Exception {
        ResponseEntity<List<Offer>> entity = this.restTemplate.exchange("/offers", HttpMethod.GET, null, new ParameterizedTypeReference<List<Offer>>() { });
        Assertions.assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);

        assertThat(entity.getBody().size(), is(0));
    }

    @Test
    public void whenCreatingOffer_thenLocationIsReturned() throws Exception {
        final Offer offer = new Offer(10L, "Grizzly Bear", "This a fiery bear",1, LocalDateTime.MAX, "EUR");

        ResponseEntity entity = this.restTemplate.postForEntity("/offers", offer, Offer.class);
        Assertions.assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        assertEquals("/offers/1", entity.getHeaders().get(HttpHeaders.LOCATION).get(0));
    }

    @Test
    public void whenCreatingOfferWithInvalidDate_then400IsReturned() throws Exception {
        final Offer offer = new Offer(10L, "Grizzly Bear", "This a fiery bear",1, LocalDateTime.MAX, "EUR");
        offer.setExpiryDate(LocalDateTime.now().minusHours(1));

        ResponseEntity<Map> entity = this.restTemplate.postForEntity("/offers", offer, Map.class);
        Assertions.assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

        assertThat((String)entity.getBody().get("message"), containsString("Expiry Date must be in the future"));
    }

    @Test
    public void whenCreatingOfferWithEmptyName_then400IsReturned() throws Exception {
        final Offer offer = new Offer(10L, null, "This a fiery bear",-1, LocalDateTime.MAX, "EUR");

        ResponseEntity<Map> entity = this.restTemplate.postForEntity("/offers", offer, Map.class);
        Assertions.assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

        assertThat((String)entity.getBody().get("message"), containsString("Validation failed"));
    }

    @Test
    public void whenCreatingOfferWithInvalidPrice_then400IsReturned() throws Exception {
        final Offer offer = new Offer(10L, "Grizzly Bear", "This a fiery bear",-1, LocalDateTime.MAX, "EUR");

        ResponseEntity<Map> entity = this.restTemplate.postForEntity("/offers", offer, Map.class);
        Assertions.assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

        assertThat((String)entity.getBody().get("message"), containsString("Validation failed"));
    }

    @Test
    public void givenThatAnOfferIsCreated_whenGettingItsLocation_thenTheOfferDetailIsReturned() throws Exception {
        final Offer offer = new Offer(1L, "Paddington Bear", "Very soft and chatty bear", 0, LocalDateTime.MAX, "EUR");

        ResponseEntity entity = this.restTemplate.postForEntity("/offers", offer, Offer.class);

        ResponseEntity<Offer> offerEntity = this.restTemplate.getForEntity(entity.getHeaders().get(HttpHeaders.LOCATION).get(0), Offer.class);
        Assertions.assertThat(offerEntity.getStatusCode()).isEqualTo(HttpStatus.OK);

        assertThat(offerEntity.getBody().getName(), is(offer.getName()));
        assertThat(offerEntity.getBody().getDescription(), is(offer.getDescription()));
    }

    @Test
    public void whenGettingAnUnexistentOffer_then404IsReturned() throws Exception {
        ResponseEntity<Offer> entity = this.restTemplate.getForEntity("/offers/123", Offer.class);

        Assertions.assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertEquals(entity.getBody(), null);
    }

    @Test
    public void givenAValidOffer_whenCancellingTheOffer_thenRetrievalGivesNotFound() throws Exception {
        final Offer offer = new Offer(1L, "Paddington Bear", "Very soft and chatty bear", 1, LocalDateTime.MAX, "EUR");

        ResponseEntity<Offer> entity = this.restTemplate.postForEntity("/offers", offer, Offer.class);

        ResponseEntity deleteEntity = this.restTemplate.exchange(entity.getHeaders().get(HttpHeaders.LOCATION).get(0), HttpMethod.DELETE, null, Void.class);
        Assertions.assertThat(deleteEntity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        ResponseEntity<Offer> offerEntity = this.restTemplate.getForEntity(entity.getHeaders().get(HttpHeaders.LOCATION).get(0), Offer.class);
        Assertions.assertThat(offerEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

}
