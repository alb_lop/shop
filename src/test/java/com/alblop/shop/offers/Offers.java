package com.alblop.shop.offers;

import java.time.LocalDateTime;

/**
 * Created by alberto.lopez on 19/11/2017.
 */
public class Offers {

    public static OfferDao expiredOffer() {
        OfferDao expiredOffer = new OfferDao();
        expiredOffer.setExpiryDate(LocalDateTime.of(2017,11,11,11,11));
        return expiredOffer;
    }

    public static OfferDao validOffer() {
        OfferDao validOffer = new OfferDao();
        validOffer.setId(3L);
        validOffer.setExpiryDate(LocalDateTime.of(LocalDateTime.now().getYear() + 1,11,11,11,11));
        return validOffer;
    }
}
