package com.alblop.shop.offers;

import org.junit.Test;

import java.time.LocalDateTime;

/**
 * Created by alberto.lopez on 20/11/2017.
 */
public class OfferTest {

    @Test(expected = IllegalArgumentException.class)
    public void whenOfferIsCreatedWithInvalidCurrency_throwsError() {
        new Offer(10L, "Bear", "This a fiery bear",1, LocalDateTime.MAX, "GBPP");
    }
}
