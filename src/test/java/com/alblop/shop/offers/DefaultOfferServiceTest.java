package com.alblop.shop.offers;

import com.alblop.shop.exceptions.InvalidOfferException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.alblop.shop.offers.Offers.expiredOffer;
import static com.alblop.shop.offers.Offers.validOffer;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class DefaultOfferServiceTest {

    @InjectMocks
    DefaultOfferService offerService;

    @Mock
    OfferRepository mockOfferRepository;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void givenThatAValidOfferExists_whenListingOffers_TheOfferIsReturned(){
        final List<OfferDao> dummyOffers = Arrays.asList(new OfferDao());
        when(mockOfferRepository.findAll()).thenReturn(dummyOffers);

        List<OfferDao> result = offerService.listOffers();

        assertThat( result, is(dummyOffers));
        verify(mockOfferRepository).findAll();
    }

    @Test
    public void givenThatAnExpiredOfferIsInTheRepository_whenListingOffers_NoOfferIsReturned(){
        final List<OfferDao> dummyOffers = Arrays.asList(expiredOffer());
        when(mockOfferRepository.findAll()).thenReturn(dummyOffers);

        List<OfferDao> result = offerService.listOffers();

        assertTrue(result.isEmpty());
        verify(mockOfferRepository).findAll();
    }


    @Test
    public void givenThatThereAreNoOffers_whenListingOffers_NoOfferIsReturned(){
        final List<OfferDao> dummyOffers = new ArrayList<>();
        when(mockOfferRepository.findAll()).thenReturn(dummyOffers);

        List<OfferDao> result = offerService.listOffers();

        assertTrue(result.isEmpty());
        verify(mockOfferRepository).findAll();
    }

    public void whenGettingAValidOffer_ReturnsOffer() {
        when(mockOfferRepository.findOne(1L)).thenReturn(validOffer());

        final Optional<OfferDao> offer = offerService.getOffer(1L);
        assertTrue(offer.isPresent());
        assertEquals(validOffer(), offer.get());
    }

    @Test(expected = InvalidOfferException.class)
    public void whenGettingANullOffer_ThrowsInvalidOffer() {
        offerService.getOffer(null);
    }

    @Test
    public void whenGettingAnExpiredOffer_ReturnsEmpty() {
        when(mockOfferRepository.findOne(1L)).thenReturn(expiredOffer());

        assertFalse(offerService.getOffer(1L).isPresent());
    }

    @Test
    public void whenCancellingAnOfferWithoutExpirationDate_ReturnsTrue() {
        when(mockOfferRepository.findOne(2L)).thenReturn(new OfferDao());

        assertTrue(offerService.cancelOffer(2L));
    }

    @Test
    public void whenCancellingANonExistentOffer_ReturnsFalse() {
        when(mockOfferRepository.findOne(2L)).thenReturn(null);

        assertFalse(offerService.cancelOffer(2L));
    }

    @Test
    public void whenCancellingAnExpiredOffer_ReturnsFalse() {
        when(mockOfferRepository.findOne(2L)).thenReturn(expiredOffer());

        assertFalse(offerService.cancelOffer(2L));
    }

    @Test(expected = InvalidOfferException.class)
    public void whenCancellingANullOffer_ThrowsInvalidOffer() {
        offerService.cancelOffer(null);
    }

    @Test
    public void whenCreatingAnOffer_ThenIdIsReturned() {
        final OfferDao validOffer = validOffer();

        when(mockOfferRepository.save(validOffer)).thenReturn(validOffer);

        assertEquals(new Long(3), offerService.createOffer(validOffer));

        verify(mockOfferRepository).save(validOffer);
    }

    @Test(expected = InvalidOfferException.class)
    public void whenCreatingAnExpiredOffer_ThrowsInvalidOffer() {
        offerService.createOffer(expiredOffer());
    }

    @Test(expected = InvalidOfferException.class)
    public void whenCreatingAnInvalidOffer_ThrowsInvalidOffer() {
        offerService.createOffer(null);
    }
}
