package com.alblop.shop.offers;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface OfferService {

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<OfferDao> listOffers();

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Optional<OfferDao> getOffer(Long id);

    @Transactional
    public Long createOffer(OfferDao map);

    @Transactional
    Boolean cancelOffer(Long id);
}
