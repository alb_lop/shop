package com.alblop.shop.offers;

import com.alblop.shop.exceptions.InvalidOfferException;

import java.time.LocalDateTime;

/**
 * Created by alberto.lopez on 20/11/2017.
 */
class OfferValidation {

    static void validateId(final Long id) {
        if(id == null || id <= 0) {
            throw new InvalidOfferException("Offer id must be positive");
        }
    }

    static void validateOffer(final OfferDao offer) {
        if(!isValidOffer(offer)) {
            throw new InvalidOfferException("Expiry Date must be in the future");
        }
    }


    static Boolean isValidOffer(final OfferDao offer) {
        return offer != null && (offer.getExpiryDate() == null || offer.getExpiryDate().isAfter(LocalDateTime.now()));
    }
}
