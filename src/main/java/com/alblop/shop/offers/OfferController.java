package com.alblop.shop.offers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/offers")
public class OfferController {

    @Autowired
    private OfferService offerService;

    @Autowired
    private ModelMapper modelMapper;

    @RequestMapping
    public List<Offer> listOffers() {
        List<OfferDao> offers = offerService.listOffers();
        return offers.stream()
                .map(offer -> modelMapper.map(offer, Offer.class))
                .collect(Collectors.toList());
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity createOffer(@Valid @RequestBody final Offer offer, HttpServletResponse response) {
        final Long id = offerService.createOffer(modelMapper.map(offer, OfferDao.class));

        return ResponseEntity.status(HttpStatus.CREATED)
                .header(HttpHeaders.LOCATION, "/offers/" + id).build();
    }

    @RequestMapping(value = "/{id}")
    public ResponseEntity<Offer> getOffer(@PathVariable final Long id) {
        Optional<OfferDao> offer = offerService.getOffer(id);
        if(!offer.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(offer.get(), Offer.class));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void cancelOffer(@PathVariable final Long id, HttpServletResponse response) {
        final Boolean cancelled = id > 0 ? offerService.cancelOffer(id) : false;
        response.setStatus(cancelled ? HttpStatus.NO_CONTENT.value() : HttpStatus.NOT_FOUND.value());
    }
}
