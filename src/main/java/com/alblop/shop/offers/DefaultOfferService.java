package com.alblop.shop.offers;

import com.google.common.collect.Streams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by alberto.lopez on 19/11/2017.
 */
@Service
public class DefaultOfferService implements OfferService {

    @Autowired
    public OfferRepository offerRepository;

    public List<OfferDao> listOffers () {
        return Streams.stream(offerRepository.findAll())
                .filter(OfferValidation::isValidOffer)
                .collect(Collectors.toList());
    }

    @Override
    public Long createOffer(OfferDao offer) {
        OfferValidation.validateOffer(offer);

        return offerRepository.save(offer).getId();
    }

    @Override
    public Boolean cancelOffer(final Long id) {
        OfferValidation.validateId(id);

        final OfferDao offer = offerRepository.findOne(id);
        if(OfferValidation.isValidOffer(offer)) {
            offer.setExpiryDate(LocalDateTime.now());
            offerRepository.save(offer);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Optional<OfferDao> getOffer(final Long id) {
        OfferValidation.validateId(id);

        OfferDao offer = offerRepository.findOne(id);
        return Optional.ofNullable(offer).filter(OfferValidation::isValidOffer);
    }

}
