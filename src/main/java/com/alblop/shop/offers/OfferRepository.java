package com.alblop.shop.offers;

/**
 * Created by alberto.lopez on 19/11/2017.
 */

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OfferRepository extends CrudRepository<OfferDao, Long> {

}
